<?php
    class ConexionMySQL{
        private $servidor;
        private $usuario;
        private $contraseña;
        private $baseDatos;
        private $puerto;
        private $conexionBD;
        private $succes;

        public function __construct(){
            $this->$servidor = "localhost";
            $this->$usuario = "php";
            $this->$contraseña = "1234";
            $this->$baseDatos = "mysql";
            $this->$puerto = 3306;
            $this->$conexionBD = "";
        }

        public function conectar(){
            $this->$conexionBD = mysqli_init();

            if(!$this->$conexionBD){
                return "Falló mysqli_init()";
            }

            if(!$this->$conexionBD -> real_connect($this->$servidor, $this->$usuario, $this->$contraseña, $this->$baseDatos, $this->$puerto)){
                return "Error de conexion, no se pudo establecer conexion con el servidor";
            }
            else {
                //$this->cerrarConexion();
                return "Éxito en la conexion ".$this->$conexionBD->host_info;
            }
        }
        
        public function consulta($consulta){
            return mysqli_real_query($this->$succes, $consulta); //bool
        }
        
        private function cerrarConexion(){
            $this->$conexionBD->close();
            //mysqli_close($this->$conexionBD);
        }

        // mysqli_get_server_info ( mysqli $link ) : string
        /*
        <?php
            echo "hola ";
            require "../conexionBD/conexionMySQL.php";
            $conexion = new ConexionMySQL();
            
            echo $conexion->conectar();

            $resultado = $conexion->consulta("INSERT INTO `empleados`(`nombre`, `rol`, `email`) VALUES ('Carlos','Motorizado','carlos2018@gmail.com')");
            echo "todo bien 2 ";
            if($resultado){
                
                echo "Se insertó correcta";
                
                /*while($fila = $resultado->fetch_row()){
                    $id = $fila['id'];
                    $usuario = $fila['usuario'];
                    $contrasena = $fila['contrasena'];
                    ?>
                    <h2><?php echo "ID: ".$id." Usuario: ".$usuario." Contraseña: ".$contrasena?></h2>
                    <?php
                }*/
            }
            else{
                echo "Parece que resultado es false";
            }
        ?>
        */
    }
?>