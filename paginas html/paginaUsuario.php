<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QueveXpress - Inicio</title>
    <link rel="stylesheet" href="../recursos css/normalize.css" />
    <link rel="stylesheet" href="../recursos css/all.min.css" />
    <link rel="stylesheet" href="../estilos css/paginaUsuario.css">
</head>
<body>
    <?php
        $servidor = "localhost";
        $usuario = "root";
        $contrasena = "root";
        $basedato = "practicasphp";
        $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basedato);
    ?>
    <main>
        <div class="panelOpciones">
            <div class="opciones">
                <a class="logotipo" href="../index.html">
                    <h1><i class="fas fa-motorcycle"></i></h1>
				    <h3>Queve<br>Xpress</h3>
                </a>
                <h1 id="tablero"><i class="far fa-chart-bar"></i></h1>
                <h1 id="pedidos"><i class="fas fa-dolly"></i></h1>
                <h1 id="empleados" class="btnPanelActivado"><i class="fas fa-users"></i></h1>
            </div>
            <div class="salir">
                <h1 id="cerrarSesion"><i class="fas fa-sign-out-alt"></i></h1>
            </div>
        </div>
        <div class="panelEmpleados">
            <h3>Empleados</h3>
            <h5>Bienvenid@, 
                <?php
                    $user = $_POST["correo"];
                    $contraseña = $_POST["contrasena"];
                    if(mysqli_connect_errno()){
                        echo "Error al conectar con la base de datos";
                        exit();
                    }
                    else{
                        $sql = "SELECT * FROM usuarios WHERE usuario='".$user."'";
                        $resultado = mysqli_query($conexion, $sql);

                        if($resultado){
                            $fila = mysqli_fetch_assoc($resultado);
                            $email = $fila['usuario'];
                            echo $email;
                        }
                        if($resultado === null) {
                            echo "El Usuario ".$user." no se encuentra registrado";
                        }
                    }
                ?>
            </h5>
            
            <div class="tablaEmpleados">
                <table>
                    <tr>
                        <th>Perfil</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th>Estado</th>
                    </tr>
                    <?php
                        /*$servidor = "localhost";
                        $usuario = "root";
                        $contrasena = "root";
                        $basedato = "practicasphp";

                        $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basedato);*/

                        if(mysqli_connect_errno()){
                            echo "Error al conectar con la base de datos";
                            exit();
                        }
                        else{
                            $sql = "SELECT * FROM empleados";
                            $resultado = mysqli_query($conexion, $sql);

                            if($resultado){
                                while($fila = mysqli_fetch_assoc($resultado)){
                                    $nombre = $fila['nombre'];
                                    $email = $fila['email'];
                                    $rol = $fila['rol'];
                                    ?>
                                    <tr>
                                        <td><i class="fas fa-user-circle"></i></td>
                                        <td><?php echo $nombre?></td>
                                        <td><?php echo $email?></td>
                                        <td><?php echo $rol?></td>
                                        <td><span class="estadoActivo">Activo</span></td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                    ?>
                    <!--<tr>
                        <td><i class="fas fa-user-circle"></i></td>
                        <td>Darlyn Buenaño</td>
                        <td>darlyn.buenano2017@uteq.edu.ec</td>
                        <td>Motorizado</td>
                        <td><span class="estadoDesctivo">Desactivo</span></td>
                    </tr>-->
                </table>
            </div>
        </div>
        
    </main>
</body>
</html>